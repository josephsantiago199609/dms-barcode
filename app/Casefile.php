<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Tag;
use App\Status;
class Casefile extends Model
{
	public function Tags()
	{
		return $this->hasMany(Tag::class);
	}

	public function Status()
	{
		return $this->hasOne(Status::class, 'id', 'status');
	}
}
