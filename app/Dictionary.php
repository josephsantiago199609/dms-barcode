<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Tag;
class Dictionary extends Model
{
    public function TagDictionary()
    {
    	return $this->hasMany(Tag::class ,'dictionary_id', 'id');
    }
}
