<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\AuditTrail;
use Artisan;
class AuditTrailController extends Controller
{
    public function index()
    {
    	$auditTrail = AuditTrail::orderBy('id', 'desc')->get();

    	return view('admin.audittrail.index', compact('auditTrail'));
    }

    public function backup(){
        Artisan::call('backup:run');
        return back();
    }
}
