<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Casefile;
use App\CasefileAttachments;
use Auth;
use App\AuditTrail;
use App\Tag;
use App\Dictionary;

class CasefileController extends Controller
{
    public function index()
    {
    	$casefiles = Casefile::all();
    	return view('admin.casefile.index', compact("casefiles"));
    }

    public function create()
    {
        $dictionaries = Dictionary::all();
    	return view('admin.casefile.create', compact("dictionaries"));
    }

    public function store(Request $request)
    {
        $user = Auth::user();
    	
    	$casefile = new CaseFile();
    	$casefile->case_title = request('caseTitle');
    	$casefile->hlurb_case_no = request('hlurbCaseNo');
    	$casefile->regional_case_no = request('regionalCaseNo');
    	$casefile->case_year = request('caseYear');
    	$casefile->nature_of_case = request('natureOfCase');
    	// $casefile->case_type = request('hlurbCaseType');
    	$casefile->other_description = request('description');
        // $casefile->assigned_to =1;
        $casefile->created_by = $user->name;
    	$casefile->save();

        $attachments = request('attachment');
        if(isset($attachments)){
            foreach ($attachments as $attachment) {
                $file = $attachment;
                $filenameWithExt = $file->getClientOriginalName();
                $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
                $extension = $file->getClientOriginalExtension();
                $fileNameToStore = $filename.'_'.time().'.'.$extension;
                $file->storeAs('public/CaseFiles/'.$casefile->id,$fileNameToStore);
                $path = 'app/public/CaseFiles/'.$casefile->id.'/'.$fileNameToStore;

                $Attachments = new CasefileAttachments;
                $Attachments->casefile_id = $casefile->id;
                $Attachments->filename = $fileNameToStore;
                $Attachments->filenameNoStamp = $filename;
                $Attachments->filepath = $path;
                $Attachments->created_by = $user->name;
                $Attachments->save();   
            }
        }

        $auditTrail = new AuditTrail;
        $auditTrail->description = '<b>'.$user->name.'</b> Added new document: <b>'.$casefile->case_title.'</b>';
        $auditTrail->created_by = $user->name;
        $auditTrail->save();

        $dictionaries = request('tags');
        if(isset($dictionaries)){
            foreach ($dictionaries as $tag) {
                $addTag = new Tag;
                $addTag->casefile_id = $casefile->id;
                $addTag->dictionary_id = $tag;
                $addTag->save();
            }
        }

    	return redirect()->route('casefile.show', $casefile->id)->with('success_alert', 'Successfully Added');
    }

     public function show($id)
    {
        $casefiles = Casefile::findorfail($id);
        $casefilesattachments = CasefileAttachments::where('casefile_id', $id)->get();

    	return view('admin.casefile.show', compact("casefiles", "casefilesattachments"));
    }

    public function edit($id)
    {
    	$casefiles = Casefile::findorfail($id);
        $casefilesattachments = CasefileAttachments::where('casefile_id', $id)->get();

        $dictionaries = Dictionary::all();

    	return view('admin.casefile.edit', compact("casefiles", "casefilesattachments","dictionaries"));
    }

    public function update(Request $request, $id)
    {
        $user = Auth::user();

    	$casefile = Casefile::findorfail($id);
    	$casefile->case_title = request('caseTitle');
    	$casefile->hlurb_case_no = request('hlurbCaseNo');
    	$casefile->regional_case_no = request('regionalCaseNo');
    	$casefile->case_year = request('caseYear');
    	$casefile->nature_of_case = request('natureOfCase');
    	$casefile->other_description = request('description');
        // $casefile->assigned_to = request('assigned_to');   
        $casefile->updated_by = $user->name; 
    	$casefile->save();

        $attachments = request('attachment');
        if(isset($attachments)){
            foreach ($attachments as $attachment) {
                $file = $attachment;
                $filenameWithExt = $file->getClientOriginalName();
                $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
                $extension = $file->getClientOriginalExtension();
                $fileNameToStore = $filename.'_'.time().'.'.$extension;
                $file->storeAs('public/CaseFiles/'.$casefile->id,$fileNameToStore);
                $path = 'app/public/CaseFiles/'.$casefile->id.'/'.$fileNameToStore;

                $Attachments = new CasefileAttachments;
                $Attachments->casefile_id = $casefile->id;
                $Attachments->filename = $fileNameToStore;
                $Attachments->filenameNoStamp = $filename;
                $Attachments->filepath = $path;
                $Attachments->created_by = $user->name;
                $Attachments->save();   

                $auditTrail = new AuditTrail;
                $auditTrail->description = '<b>'.$user->name.'</b> Attached file: <b>'.$filename.'</b> for Case: <b>'.$casefile->case_title.'</b>';
                $auditTrail->created_by = $user->name;
                $auditTrail->save();
            }
        }

        $auditTrail = new AuditTrail;
        $auditTrail->description = '<b>'.$user->name.'</b> Updated description of Case: <b>'.$casefile->case_title.'</b>';
        $auditTrail->created_by = $user->name;
        $auditTrail->save();

        $dictionaries = request('tags');
        $deleteTags = Tag::where('casefile_id', $casefile->id)->delete();
        if(isset($dictionaries)){
            foreach ($dictionaries as $tag) {
                $addTag = new Tag;
                $addTag->casefile_id = $casefile->id;
                $addTag->dictionary_id = $tag;
                $addTag->save();
            }
        }
    	return redirect()->route('casefile.show', $casefile->id)->with('success_alert', 'Successfully Updated');
    }

    public function download($id)
    {
    	$attachment = CasefileAttachments::findorfail($id);
        $casefile= Casefile::findorfail($attachment->casefile_id);
        $user = Auth::user();
        $auditTrail = new AuditTrail;
        $auditTrail->description = '<b>'.$user->name.'</b> Downloaded file: <b>'.$attachment->filenameNoStamp.'</b> from Case: <b>'.$casefile->case_title.'</b>';
        $auditTrail->created_by = $user->name;
        $auditTrail->save();

    	return response()->download(storage_path($attachment->filepath));
    }

    public function assign($id)
    {
        $casefiles = Casefile::findorfail($id);

        return view('admin.casefile.assign', compact("casefiles"));
    }

    public function assignupdate(Request $request, $id)
    {
        $user = Auth::user();
        $casefile = Casefile::findorfail($id);
        $casefile->assigned_to = request('assigned_to');    
        $casefile->updated_by = $user->name;
        $casefile->save();

        $auditTrail = new AuditTrail;
        $auditTrail->description = '<b>'.$user->name.'</b> Assigned Case: <b>'.$casefile->case_title.' </b> to <b>' .$casefile->assigned_to.'</b>';
        $auditTrail->created_by = $user->name;
        $auditTrail->save();
        return redirect()->route('casefile.index')->with('success_alert', 'Successfully Updated');
    }

    public function deleteattachment($id, $file_id)
    {   
        $user = Auth::user();
        $casefiles = Casefile::findorfail($id);
        $attachment = CasefileAttachments::findorfail($file_id);
        Storage::delete('public/CaseFiles/'.$casefiles->id.'/'.$attachment->filename);
        $casefiles->updated_by = $user->name;
        $casefiles->save();

        $auditTrail = new AuditTrail;
        $auditTrail->description = '<b>'.$user->name.'</b> Deleted attachment: <b>'.$attachment->filenameNoStamp.'</b> from Case: '.$casefiles->case_title.'</b>';
        $auditTrail->created_by = $user->name;
        $auditTrail->save();

        $attachment->delete();

        return redirect()->back()->with('success_alert', 'Attachment Successfully Removed');
    }
}
