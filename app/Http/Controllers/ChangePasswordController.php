<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Auth;
use Hash;
class ChangePasswordController extends Controller
{
    public function index()
    {
    	$changepass = User::all();
    	return view('admin.changepass.index', compact("changepass"));
    }

    public function update(Request $request)
    {
    	$this->validate($request, [
    		'old_password' => 'required',
			'new_password' => 'min:6|required_with:confirm_password|same:confirm_password',
			'confirm_password' => 'required'
		]);
    	$user = Auth::user();

		if(Hash::check($request->old_password, $user->password)) {
        	$updateUser = User::find($user->id);
        	$updateUser->password = bcrypt($request->new_password);
        	$updateUser->save();
        	return back()->with('success_alert','Password changed successfully');
    	} else {
        	return back()->with('alert','Old password and current password does not match');
    	}
    }
}
