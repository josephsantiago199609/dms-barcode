<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Dictionary;
class DictionaryController extends Controller
{
    public function store()
    {

    	$dictionary = new Dictionary;
    	$dictionary->tag = request('dictionary');
    	$dictionary->save();

    	return ['id'=>$dictionary->id,'tag'=>$dictionary->tag];
    }
}
