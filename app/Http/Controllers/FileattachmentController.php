<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Casefile;

class FileattachmentController extends Controller
{
    public function store(Request $request)
    {
    	if($request->hasFile('attachment')){
    		foreach($request->file('attachment') as $file){
    			$filenameWithExt = $request->file('attachment')->getClientOriginalName();
	    		$filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
	    		$extension = $request->file('attachment')->getClientOriginalExtension();
	    		$fileNameToStore = $filename.'_'.time().'.'.$extension;
	    		$path = $request->file('attachment')->storeAs('public/attachment', $fileNameToStore);
	    		$originalpath = 'app/public/attachment/'.$fileNameToStore;
	    		$data[] = $fileNameToStore; 

    		}
    		
    	}else{
    		$fileNameToStore ="noimage.jpg";
    	}

    	$fileattachment = new FileAttachment();
    	$fileattachment->attachment=json_encode($data);
    	$fileattachment->file_name = $fileNameToStore;
    	$fileattachmente->file_path = $originalpath;
    	$fileattachment->filenameNoStamp = $fileNameToStore;
    	$fileattachment->save();

    	
    }
    public function download($casefile->id)
    {
    	$fileattachments = Fileattachment::findorfail($id);

    	return response()->download(storage_path($fileattachments->file_path));
    }
}
