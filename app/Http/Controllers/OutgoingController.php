<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\outgoing;
use App\OutgoingAttachments;
use Auth;
use Illuminate\Support\Facades\Storage;
use App\AuditTrail;

class OutgoingController extends Controller
{
    public function index()
    {
    	$outgoingfiles = Outgoing::all();
    	return view('admin.outgoing.index', compact ("outgoingfiles"));
    }

    public function create()
    {
        
        $last_row=Outgoing::latest()->first();
            if($last_row){
                $increment=$last_row->barcode_number + 1;
            }else{
                $increment = 1;
            }
            
    	return view('admin.outgoing.create', compact ("increment"));
    }

    public function store(Request $request)
    {
        $increment;
        $user = Auth::user();
    	$outgoingfile = new Outgoing();
        $outgoingfile->barcode_number = request('barcode_number');
    	$outgoingfile->title = request('title');
    	$outgoingfile->description = request('description');
    	$outgoingfile->from = request('from');
 		$outgoingfile->created_by = $user->name;
    	$outgoingfile->save();
        
        $attachments = request('attachment');
        if(isset($attachments)){
            foreach ($attachments as $attachment) {
                $file = $attachment;
                $filenameWithExt = $file->getClientOriginalName();
                $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
                $extension = $file->getClientOriginalExtension();
                $fileNameToStore = $filename.'_'.time().'.'.$extension;
                $file->storeAs('public/Outgoing/'.$outgoingfile->id,$fileNameToStore);
                $path = 'app/public/Outgoing/'.$outgoingfile->id.'/'.$fileNameToStore;

                $Attachments = new OutgoingAttachments;
                $Attachments->outgoings_id = $outgoingfile->id;
                $Attachments->filename = $fileNameToStore;
                $Attachments->filenameNoStamp = $filename;
                $Attachments->filepath = $path;
                $Attachments->created_by = $user->name;
                $Attachments->save();   
            }
        }

        $auditTrail = new AuditTrail;
        $auditTrail->description = '<b>'.$user->name.'</b> Added new Outgoing Document/s: <b>'.$outgoingfile->description.'</b>';
        $auditTrail->created_by = $user->name;
        $auditTrail->save();
    	return redirect()->route('outgoing.show', $outgoingfile->id)->with('success_alert', 'Added Successfully');
    }

     public function show($id)
    {
    	$outgoingfiles = Outgoing::findorfail($id);
        $attachments = OutgoingAttachments::where('outgoings_id', $outgoingfiles->id)->get();
    	return view('admin.outgoing.show', compact("outgoingfiles", "attachments"));
    }

    public function edit($id)
    {
        
    	$outgoingfiles = Outgoing::findorfail($id);
        $attachments = OutgoingAttachments::where('outgoings_id', $outgoingfiles->id)->get();
    	return view('admin.outgoing.edit', compact("outgoingfiles" ,"attachments"));
    }

     public function update(Request $request, $id)
    {
        $user = Auth::user();
        
    	$outgoingfile = Outgoing::findorfail($id);
    	$outgoingfile->title = request('title');
    	$outgoingfile->description = request('description');
    	$outgoingfile->from = request('from');
        $outgoingfile->updated_by = $user->name;
    	$outgoingfile->save();

        $attachments = request('attachment');
        if(isset($attachments)){
            foreach ($attachments as $attachment) {
                $file = $attachment;
                $filenameWithExt = $file->getClientOriginalName();
                $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
                $extension = $file->getClientOriginalExtension();
                $fileNameToStore = $filename.'_'.time().'.'.$extension;
                $file->storeAs('public/Outgoing/'.$outgoingfile->id,$fileNameToStore);
                $path = 'app/public/Outgoing/'.$outgoingfile->id.'/'.$fileNameToStore;

                $Attachments = new OutgoingAttachments;
                $Attachments->outgoings_id = $outgoingfile->id;
                $Attachments->filename = $fileNameToStore;
                $Attachments->filenameNoStamp = $filename;
                $Attachments->filepath = $path;
                $Attachments->created_by = $user->name;
                $Attachments->save(); 

                $auditTrail = new AuditTrail;
                $auditTrail->description = '<b>'.$user->name.'</b> Attached file: <b>'.$filename.'</b> from Outgoing Document/s: <b>'.$outgoingfile->description.'</b>';
                $auditTrail->created_by = $user->name;
                $auditTrail->save(); 
            }
        }

        $auditTrail = new AuditTrail;
        $auditTrail->description = '<b>'.$user->name.'</b> Updated Outgoing Document/s: <b>'.$outgoingfile->description.'</b>';
        $auditTrail->created_by = $user->name;
        $auditTrail->save();
    	return redirect()->route('outgoing.show', $outgoingfile->id)->with('success_alert', 'Updated Successfully');
    }

     public function download($id)
    {
        $user = Auth::user();
    	$outgoingfiles = OutgoingAttachments::findorfail($id);
        $outgoingfile = Outgoing::findorfail($outgoingfiles->outgoings_id);

        $auditTrail = new AuditTrail;
        $auditTrail->description = '<b>'.$user->name.'</b> Downloaded file: <b>'.$outgoingfiles->filenameNoStamp.'</b> from Outgoing Document/s: <b>'.$outgoingfile->description.'</b>';
        $auditTrail->created_by = $user->name;
        $auditTrail->save(); 

    	return response()->download(storage_path($outgoingfiles->filepath));
    }

    public function deleteattachment($id, $file_id)
    {   
        $user = Auth::user();
        $outgoingfile = Outgoing::findorfail($id);
        $outgoingfile->updated_by = $user->name;
        $outgoingfile->save();
        $attachment = OutgoingAttachments::findorfail($file_id);
        Storage::delete('public/Outgoing/'.$outgoingfile->id.'/'.$attachment->filename);

        $auditTrail = new AuditTrail;
        $auditTrail->description = '<b>'.$user->name.'</b> Deleted file: <b>'.$attachment->filenameNoStamp.'</b> from Outgoing Document/s: <b>'.$outgoingfile->description.'</b>';
        $auditTrail->created_by = $user->name;
        $auditTrail->save();
        
        $attachment->delete();

        return redirect()->back()->with('success_alert', 'Attachment Successfully Removed');
    }
}
