<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Receivingfile;
use App\ReceivingfileAttachments;
use Auth;
use Illuminate\Support\Facades\Storage;
use App\AuditTrail;

class ReceivingfileController extends Controller
{
    public function index()
    {
    	$receivingfiles = Receivingfile::all();
    	return view('admin.receiving.index', compact ("receivingfiles"));
    }

    public function create()
    {
    	return view('admin.receiving.create');
    }

    public function store(Request $request)
    {
        $user = Auth::user();

    	$receivingfile = new Receivingfile();
    	$receivingfile->title = request('title');
    	$receivingfile->description = request('description');
    	$receivingfile->from = request('from');
        $receivingfile->created_by = $user->name;
    	$receivingfile->save();
        
        $attachments = request('attachment');
        if(isset($attachments)){
            foreach ($attachments as $attachment) {
                $file = $attachment;
                $filenameWithExt = $file->getClientOriginalName();
                $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
                $extension = $file->getClientOriginalExtension();
                $fileNameToStore = $filename.'_'.time().'.'.$extension;
                $file->storeAs('public/Incoming/'.$receivingfile->id,$fileNameToStore);
                $path = 'app/public/Incoming/'.$receivingfile->id.'/'.$fileNameToStore;

                $Attachments = new ReceivingfileAttachments;
                $Attachments->receivingfiles_id = $receivingfile->id;
                $Attachments->filename = $fileNameToStore;
                $Attachments->filenameNoStamp = $filename;
                $Attachments->filepath = $path;
                $Attachments->created_by = $user->name;
                $Attachments->save();   
            }
        }

        $auditTrail = new AuditTrail;
        $auditTrail->description = '<b>'.$user->name.'</b> Added new Incoming Document/s: <b>'.$receivingfile->description.'</b>';
        $auditTrail->created_by = $user->name;
        $auditTrail->save();

    	return redirect()->route('receivingfile.show', $receivingfile->id)->with('success_alert', 'Added Successfully');
    }

    public function show($id)
    {
    	$receivingfiles = Receivingfile::findorfail($id);
        $attachments = ReceivingfileAttachments::where('receivingfiles_id', $receivingfiles->id)->get();
    	return view('admin.receiving.show', compact("receivingfiles","attachments"));
    }

    public function edit($id)
    {
    	$receivingfiles = Receivingfile::findorfail($id);
        $attachments = ReceivingfileAttachments::where('receivingfiles_id', $receivingfiles->id)->get();
    	return view('admin.receiving.edit', compact("receivingfiles", "attachments"));
    }

    public function update(Request $request, $id)
    {
        $user = Auth::user();

    	$receivingfile = Receivingfile::findorfail($id);
    	$receivingfile->title = request('title');
    	$receivingfile->description = request('description');
    	$receivingfile->from = request('from');
        $receivingfile->updated_by = $user->name;
    	$receivingfile->save();

        $attachments = request('attachment');
        if(isset($attachments)){
            foreach ($attachments as $attachment) {
                $file = $attachment;
                $filenameWithExt = $file->getClientOriginalName();
                $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
                $extension = $file->getClientOriginalExtension();
                $fileNameToStore = $filename.'_'.time().'.'.$extension;
                $file->storeAs('public/Incoming/'.$receivingfile->id,$fileNameToStore);
                $path = 'app/public/Incoming/'.$receivingfile->id.'/'.$fileNameToStore;

                $Attachments = new ReceivingfileAttachments;
                $Attachments->receivingfiles_id = $receivingfile->id;
                $Attachments->filename = $fileNameToStore;
                $Attachments->filenameNoStamp = $filename;
                $Attachments->filepath = $path;
                $Attachments->created_by = $user->name;
                $Attachments->save();  

                $auditTrail = new AuditTrail;
                $auditTrail->description = '<b>'.$user->name.'</b> Attached file: <b>'.$filename.'</b> from Incoming Document/s: <b>'.$receivingfile->description.'</b>';
                $auditTrail->created_by = $user->name;
                $auditTrail->save(); 
            }
        }

        $auditTrail = new AuditTrail;
        $auditTrail->description = '<b>'.$user->name.'</b> Updated Incoming Document/s: <b>'.$receivingfile->description.'</b>';
        $auditTrail->created_by = $user->name;
        $auditTrail->save();
    	return redirect()->route('receivingfile.show', $receivingfile->id)->with('success_alert', 'Updated Successfully');
    }
     public function download($id)
    {
        $user = Auth::user();

    	$receivingfiles = ReceivingfileAttachments::findorfail($id);

        $receivingfile = Receivingfile::findorfail($receivingfiles->receivingfiles_id);
        $auditTrail = new AuditTrail;
        $auditTrail->description = '<b>'.$user->name.'</b> Downloaded file: <b>'.$receivingfiles->filenameNoStamp.'</b> from Incoming Document/s: <b>'.$receivingfile->description.'</b>';
        $auditTrail->created_by = $user->name;
        $auditTrail->save(); 

    	return response()->download(storage_path($receivingfiles->filepath));
    }

    public function deleteattachment($id, $file_id)
    {   
        $user = Auth::user();
        $receivingfile = Receivingfile::findorfail($id);
        $receivingfile->updated_by = $user->name;
        $receivingfile->save();
        $attachment = ReceivingfileAttachments::findorfail($file_id);
        Storage::delete('public/Incoming/'.$receivingfile->id.'/'.$attachment->filename);

        $auditTrail = new AuditTrail;
        $auditTrail->description = '<b>'.$user->name.'</b> Deleted file: <b>'.$attachment->filenameNoStamp.'</b> from Incoming Document/s: <b>'.$receivingfile->description.'</b>';
        $auditTrail->created_by = $user->name;
        $auditTrail->save(); 

        $attachment->delete();

        return redirect()->back()->with('success_alert', 'Attachment Successfully Removed');
    }
}
