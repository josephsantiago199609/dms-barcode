<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Report;
use App\Casefile;
use App\Status;
use App\StatusLog;
use App\StatusLogAttachemt;
use Auth;
use App\AuditTrail;

class ReportController extends Controller
{
    public function index()
    {
    	$casefiles = Casefile::all();
    	return view('admin.reports.index', compact ("casefiles"));
    }

    public function show($id)
    {
    	
    	$casefiles = Casefile::findorfail($id);
        $statuses = Status::all();
        $statusLog = StatusLog::where('casefile_id', $casefiles->id)->get();
    	return view('admin.reports.show', compact("casefiles","statuses","statusLog"));
    }

    public function print($id)
    {
        
        $casefiles = Casefile::findorfail($id);
        $statuses = Status::all();
        $statusLog = StatusLog::where('casefile_id', $casefiles->id)->get();
        return view('admin.reports.print', compact("casefiles","statuses","statusLog"));
    }

    public function changeStatus($id)
    {
        $user = Auth::user();
        $casefiles = Casefile::findorfail($id);

        $statusLogCount = StatusLog::where('casefile_id', $casefiles->id)
                                ->where('status_id', request('status_id'))
                                ->count();

        $statusLog = new StatusLog;
        $statusLog->casefile_id = $casefiles->id;
        $statusLog->status_id = request('status_id');
        $statusLog->remarks = request('remarks');
        $statusLog->count = $statusLogCount + 1;
        $statusLog->created_by = $user->name;
        $statusLog->save();

        $casefiles->status = request('status_id');
        $casefiles->save();

        $attachments = request('attachment');
        if(isset($attachments)){
            foreach ($attachments as $attachment) {
                $file = $attachment;
                $filenameWithExt = $file->getClientOriginalName();
                $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
                $extension = $file->getClientOriginalExtension();
                $fileNameToStore = $filename.'_'.time().'.'.$extension;
                $file->storeAs('public/StatusLog/'.$casefiles->id.'/'.$statusLog->id,$fileNameToStore);
                $path = 'app/public/StatusLog/'.$casefiles->id.'/'.$statusLog->id.'/'.$fileNameToStore;

                $Attachments = new StatusLogAttachemt;
                $Attachments->status_log_id = $statusLog->id;
                $Attachments->filename = $fileNameToStore;
                $Attachments->filenameNoStamp = $filename;
                $Attachments->filepath = $path;
                $Attachments->created_by = $user->name;
                $Attachments->save();   
            }
        }
        $auditTrail = new AuditTrail;
        $auditTrail->description = '<b>'.$user->name.'</b> Updated Status for Document: <b>'.$casefiles->case_title.'</b> to ' . '<b>' .$casefiles->Status->status. '</b>';
        $auditTrail->created_by = $user->name;
        $auditTrail->save();

        return redirect()->route('reports.show', $casefiles->id)->with('success_alert', 'Status Changed Successfully');
    }

    public function download($id,$attachment_id)
    {
        $attachment = StatusLogAttachemt::findorfail($attachment_id);
        $casefile= Casefile::findorfail($id);

        $user = Auth::user();
        $auditTrail = new AuditTrail;
        $auditTrail->description = '<b>'.$user->name.'</b> Downloaded file: <b>'.$attachment->filenameNoStamp.'</b> from Status Report: <b>'.$casefile->case_title.'</b>';
        $auditTrail->created_by = $user->name;
        $auditTrail->save();

        return response()->download(storage_path($attachment->filepath));
    }
}
