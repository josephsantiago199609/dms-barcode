<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Status;
use Auth;

class StatusController extends Controller
{
     public function index()
    {
    	$statuses = Status::all();
    	return view('admin.status.index', compact ("statuses"));
    }

    public function create()
    {
    	return view('admin.status.create');
    }

    public function edit($id)
    {
    	$statuses = Status::findorfail($id);
    	return view('admin.status.edit', compact("statuses"));
    }

    public function store(Request $request)
    {
        $user = Auth::user();

    	$statuses = new Status();
    	$statuses->status = request('status');
 		$statuses->created_by = $user->name;
    	$statuses->save();

    	$statuses = Status::all();
    	return redirect()->route('status.index')->with('success_alert', 'Added Successfully');
    	
    }

    public function update(Request $request, $id)
    {
        $user = Auth::user();

    	$statuses = Status::findorfail($id);
    	$statuses->status = request('status');
    	$statuses->save();

    	return redirect()->route('status.index')->with('success_alert', 'Updated Successfully');
    }
}
