<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Status;
use App\StatusLogAttachemt;
class StatusLog extends Model
{
    public function Status()
    {
    	return $this->hasOne(Status::class, 'id', 'status_id');
    }

    public function Attachments()
    {
    	return $this->hasMany(StatusLogAttachemt::class , 'status_log_id','id');
    }
}
