<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Dictionary;

class Tag extends Model
{
    public function Dictionary()
    {
    	return $this->hasOne(Dictionary::class, 'id','dictionary_id');
    }
}
