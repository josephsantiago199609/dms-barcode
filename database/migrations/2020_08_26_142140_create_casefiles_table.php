<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCasefilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('casefiles', function (Blueprint $table) {
            $table->increments('id');
            $table->text('case_title')->nullable();
            $table->text('hlurb_case_no')->nullable();
            $table->text('regional_case_no')->nullable();
            $table->text('case_year')->nullable();
            $table->text('nature_of_case')->nullable();
            $table->text('other_description')->nullable();
            $table->text('status')->nullable();
            $table->text('statusremarks')->nullable();
            $table->text('assigned_to')->nullable();    
            $table->text('deliberated')->nullable();
            $table->string('filename')->nullable();
            $table->string('filenameNoStamp')->nullable();
            $table->string('filepath')->nullable();
            $table->string('filesize')->nullable();
            $table->string('created_by')->nullable();
            $table->string('updated_by')->nullable();
            $table->string('deleted_by')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('casefiles');
    }
}
