<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOutgoingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('outgoings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('barcode_number')->nullable();
            $table->text('title')->nullable();
            $table->text('description')->nullable();
            $table->text('type')->nullable();
            $table->text('from')->nullable();
            $table->string('filename')->nullable();
            $table->string('filenameNoStamp')->nullable();
            $table->string('filepath')->nullable();
            $table->string('filesize')->nullable();
            $table->string('created_by')->nullable();
            $table->string('updated_by')->nullable();
            $table->string('deleted_by')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('outgoings');
    }
}
