<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCasefileAttachmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('casefile_attachments', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('casefile_id');
            $table->string('filename')->nullable();
            $table->string('filenameNoStamp')->nullable();
            $table->string('filepath')->nullable();
            $table->string('filesize')->nullable();
            $table->string('created_by');
            $table->string('updated_by')->nullable();
            $table->string('deleted_by')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('casefile_attachments');
    }
}
