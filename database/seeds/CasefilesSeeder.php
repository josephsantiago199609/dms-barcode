<?php

use Illuminate\Database\Seeder;

class CasefilesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('casefiles')->insert([
            'case_title' => 'Rolando Samoza v. Phinma Properties',
            'hlurb_case_no' => 'HSAC-REM-A-200124-0219',
            'assigned_to' => '3',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);
    }
}
