<?php

use Illuminate\Database\Seeder;

class InitialUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
        [
        	'name' => 'Administrator',
        	'username' => 'admin',
        	'password' => bcrypt('P@ssw0rd')
        ],
      
    ]);

    }
}
