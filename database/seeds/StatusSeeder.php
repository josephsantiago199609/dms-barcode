<?php

use Illuminate\Database\Seeder;

class StatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('statuses')->insert([
	        [
	        	'status' => 'Review in Progress'	
	        ],
	        [
	        	'status' => 'Drafting in Progress' 	
	        ],
	        [
	        	'status' => 'For Finalization'	
	        ],
	        [
	        	'status' => 'For Consultation with Comm. FJE'	
	        ],
	        [
	        	'status' => 'Final Copy is Ready'	
	        ],
	        [
	        	'status' => 'Transmitted to all Commissioner'	
	        ],
	        [
	        	'status' => 'Transmitted to Office of Comm. Pintor'	
	        ],
	        [
	        	'status' => 'Transmitted to Office of Comm. Galicia'	
	        ],
	        [
	        	'status' => 'Transmitted to Office of Comm. Golez-Cabrera'	
	        ],
	        [
	        	'status' => 'Completed'	
	        ],
	    ]);
    }
}
