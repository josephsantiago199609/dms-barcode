@extends('layouts.app')
@section('content')
@if (session('success_alert'))
<br>
    <div class="alert alert-success">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        {{ session('success_alert') }}
    </div>
@endif
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<br>
<div class="card">
	<div class="card-body">
		<div class="row">
			<div class="col-md-6">
				<h4>User Logs</h4>
			</div>
		</div><br>
		<div class="row">
        	<div class="col">
	        	<table class="table table-striped hover table-bordered" id="table">
		            <thead>
		                <tr>
		                    <!-- <th>Title</th> -->
		                    <th>Logs</th>
		                    <th>Date and Time</th>
		                    
		                </tr>
		            </thead>
		            <tbody>
		            	@foreach($auditTrail as $log)
		            		<tr>
		            			<td>
		            				{!! $log->description !!} 
		            			</td>
		            			<td>{{ date('F d, Y h:i:s a', strtotime($log->created_at))}} </td>
		            		</tr>
		            	@endforeach
		            </tbody>
		         </table>
	        </div>	
		</div>
		

		
	</div>
</div>
 <script type="text/javascript" src="https://code.jquery.com/jquery-3.5.1.js"></script>
 <script type="text/javascript" src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript">
	 $(document).ready( function () {
	    $('#table').DataTable({
	    	"ordering": false
	    });
	    } );

	</script>

@endsection