@extends('layouts.app')
@section('content')
@if (session('success_alert'))
    <div class="alert alert-success">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        {{ session('success_alert') }}
    </div>
@endif
<form method="post" action="{{route('casefile.assignupdate', $casefiles->id)}}">
	{{ csrf_field() }}
	<br>
	<div class="card">
		<div class="card-body">
			<button type="submit" class="btn-sm btn btn-success" style="float: right;" id="submit">Save</button>
			<h4><b>Assign</b></h4><br>

			<form>
			   <div class="input-group mb-3">
				  <div class="input-group-prepend">
				    <label class="input-group-text" for="inputGroupSelect01">Assign to:</label>
				  </div>
				  <select class="custom-select" id="inputGroupSelect01" name="assigned_to">
				    <option selected>{{$casefiles->assigned_to}}</option>
				    <option value="1">1</option>
				    <option value="2">2</option>
				    <option value="3">3</option>
				    <option value="4">4</option>
				    
				    
				  </select>
				</div>
			</form>
		</div>
	</div>
	
</form>
@endsection