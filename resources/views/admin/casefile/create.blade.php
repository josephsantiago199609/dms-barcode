@extends('layouts.app')
@section('content')
@if (session('success_alert'))
    <div class="alert alert-success">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        {{ session('success_alert') }}
    </div>
@endif
<form method="post" action="{{route('casefile.store')}}" enctype="multipart/form-data">
	{{ csrf_field() }}
	<br>
	<div class="card">
		<div class="card-body">
			<button type="submit" class="btn-sm btn btn-success" style="float: right;" id="submit">Submit</button>
			<h4><b>Upload Document File</b></h4><br>

			
			  <div class="form-group row">
			    <label for="caseFile" class="col-sm-2 col-form-label">Document Title:</label>
			    <div class="col-sm-10">
			      <input type="text" class="form-control" id="caseTitle" name="caseTitle" required="">
			    </div>
			  </div>
			  <div class="form-group row">
			    <label for="hlurbCaseNo" class="col-sm-2 col-form-label">Document No:</label>
			    <div class="col-sm-10">
			      <input type="text" class="form-control" id="hlurbCaseNo" name="hlurbCaseNo">
			    </div>
			  </div>
			 <!--  <div class="form-group row">
			    <label for="regionalCaseNo" class="col-sm-2 col-form-label">Regional Case No:</label>
			    <div class="col-sm-10">
			      <input type="text" class="form-control" id="regionalCaseNo" name="regionalCaseNo">
			    </div>
			  </div> -->
			  <div class="form-group row">
			    <label for="caseYear" class="col-sm-2 col-form-label">Date:</label>
			    <div class="col-sm-4">
			      <input type="date" class="form-control" id="caseYear" name="caseYear">
			    </div>
			    <!-- <label for="hlurbCaseType" class="col-sm-2 col-form-label"><b>Case Type:</b></label>
			    <div class="col-sm-4">
			      <input type="text" class="form-control" id="hlurbCaseType" name="hlurbCaseType">
			    </div> -->
			  </div>
			   <!-- <div class="form-group row">
			    <label for="caseFile" class="col-sm-2 col-form-label">Description:</label>
			    <div class="col-sm-10">
			      <input type="text" class="form-control" id="natureOfCase" name="natureOfCase">
			    </div>
			  </div> -->
			   <div class="form-group row">
			    <label for="caseFile" class="col-sm-2 col-form-label">Description:</label>
			    <div class="col-sm-10">
			      <textarea class="form-control" id="description" name="description" rows="4"></textarea>
			    </div>
			  </div>
			  <div class="form-group row">
			    <label for="tags" class="col-sm-2 col-form-label">Tags:</label>
			    <div class="col-sm-8">
				    <select class="js-example-basic-multiple form-control" name="tags[]" multiple="multiple" id="tags">
					  @foreach($dictionaries as $dictionary)
						  <option value="{{$dictionary->id}}">{{$dictionary->tag}}</option>
					  @endforeach
					</select>
					<br>
					<!-- Button trigger modal -->
					<a href="#" class="float-left" id="addDictionary" data-toggle="modal" data-target="#exampleModalCenter" style="font-size: 12px;">
					  Add Dictionary
					</a>
				</div>
			  </div>
			  <div class="form-group row">
			    <label for="attachment" class="col-sm-2 col-form-label">Attachment:</label>
			    <div class="col-sm-10">
			      <input type="file" id="attachment" name="attachment[]" multiple></textarea>
			    </div>
			  </div>	   
		</div>
	</div>
	
</form>
<!-- Modal -->
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Add Dictionary for Tags</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

        <div class="modal-body">
	        <div class="form-group row">
			    <label for="dictionary" class="col-sm-2 col-form-label">Tag:</label>
			    <div class="col-sm-10">
			    	<input type="text" class="form-control" id="dictionaryVal" name="dictionary">
			    </div>
			</div>
        </div>
        <div class="modal-footer">
	        <button type="button" class="btn btn-secondary" data-dismiss="modal" id="closeModal">Close</button>
	        <button type="button" class="btn btn-primary" id="saveDictionary">Save changes</button>
	    </div>
    </div>
  </div>
</div>
<script type="text/javascript" src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script>
	$(document).ready(function() {
	    $('.js-example-basic-multiple').select2();

	    $('#addDictionary').click(function(){
	    	$('.modal-body').find('textarea,input').val('');
	    });
	    
	    $('#saveDictionary').click(function() {
	    	var dictionaryVal = $('#dictionaryVal').val();

	    	if( dictionaryVal != ''){
	    		$.ajax({
				type: "POST",
				url: "../../dictionary",
				data: {dictionary:dictionaryVal},
				headers: {
			        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			    },
				success: function(data){
					console.log(data.id)
					$('#tags').append( "<option value='"+ data.id +"' selected=''>"+ data.tag +"</option>");
				}		
			})
	    	$('#dictionaryVal').val('');
	    	// $('#closeModal').click();
	    	}
	    	
	    })
	});
</script>
@endsection