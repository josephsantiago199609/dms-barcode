@extends('layouts.app')
@section('content')
@if (session('success_alert'))
    <div class="alert alert-success">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        {{ session('success_alert') }}
    </div>
@endif
<form method="post" action="{{route('casefile.update', $casefiles->id)}}" enctype="multipart/form-data">
	{{ csrf_field() }}
	<br>
	<div class="card">
		<div class="card-body">
			<button type="submit" class="btn-sm btn btn-success" style="float: right;" id="submit">Update</button>
			<h4><b>Upload Document File</b></h4><br>

			<form>
			  <div class="form-group row">
			    <label for="caseFile" class="col-sm-2 col-form-label"><b>Document Title:</b></label>
			    <div class="col-sm-10">
			      <input type="text" class="form-control" id="caseTitle" name="caseTitle" value="{{$casefiles->case_title}}">
			    </div>
			  </div>
			  <div class="form-group row">
			    <label for="hlurbCaseNo" class="col-sm-2 col-form-label"><b>Document No:</b></label>
			    <div class="col-sm-10">
			      <input type="text" class="form-control" id="hlurbCaseNo" name="hlurbCaseNo" value="{{$casefiles->hlurb_case_no}}">
			    </div>
			  </div>
			  <!-- <div class="form-group row">
			    <label for="regionalCaseNo" class="col-sm-2 col-form-label"><b>Regional Case No:</b></label>
			    <div class="col-sm-10">
			      <input type="text" class="form-control" id="regionalCaseNo" name="regionalCaseNo" value="{{$casefiles->regional_case_no}}">
			    </div>
			  </div> -->
			 <!--  <div class="form-group row">
			    <label for="caseYear" class="col-sm-2 col-form-label"><b>Case Year:</b></label>
			    <div class="col-sm-4">
			      <input type="date" class="form-control" id="caseYear" name="caseYear" value="{{$casefiles->case_year}}">
			    </div>
			     <label for="hlurbCaseType" class="col-sm-2 col-form-label"><b>Case Type:</b></label>
			    <div class="col-sm-4">
			      <input type="text" class="form-control" id="hlurbCaseType" name="hlurbCaseType">
			    </div> 
			  </div> -->
			   <!-- <div class="form-group row">
			    <label for="caseFile" class="col-sm-2 col-form-label"><b>Nature of case:</b></label>
			    <div class="col-sm-10">
			      <input type="text" class="form-control" id="natureOfCase" name="natureOfCase" value="{{$casefiles->nature_of_case}}">
			    </div>
			  </div> -->
			   <div class="form-group row">
			    <label for="caseFile" class="col-sm-2 col-form-label"><b>Description:</b></label>
			    <div class="col-sm-10">
			      <textarea class="form-control" id="description" name="description" rows="4">{{$casefiles->other_description}}</textarea>
			    </div>
			  </div>
			  <div class="form-group row">
			    <label for="tags" class="col-sm-2 col-form-label">Tags:</label>
			    <div class="col-sm-8">
				    <select class="js-example-basic-multiple form-control" name="tags[]" multiple="multiple" id="tags">
					  @foreach($dictionaries as $dictionary)
					  	@if($dictionary->TagDictionary->where('casefile_id','=', $casefiles->id)->count())
						  <option value="{{$dictionary->id}}" selected="">{{$dictionary->tag}}</option>
						@else
						  <option value="{{$dictionary->id}}">{{$dictionary->tag}}</option>
						@endif
					  @endforeach
					  
					</select>
					<br>
					<!-- Button trigger modal -->
					<a href="#" class="float-left" id="addDictionary" data-toggle="modal" data-target="#exampleModalCenter" style="font-size: 12px;">
					  Add Dictionary
					</a>
				</div>
			  </div>
			  <div class="form-group row">
			    <label for="attachment" class="col-sm-2 col-form-label">Attachment:</label>
			    <div class="col-sm-10">
			      <input type="file" id="attachment" name="attachment[]" multiple></textarea>
			    </div>
			  </div>

			</form>
		</div>
	</div>
	<div class="card">
		<div class="card-body">
			<h4>Attachments</h4><br>
			<br>
			<table class="table table-hover">
				<thead>
					<tr>
						<th>Filename</th>
						<th>Actions</th>
					</tr>
				</thead>
				<tbody>
				@foreach($casefilesattachments as $attachment)
				<tr>
					<td>{{$attachment->filenameNoStamp}}</td>
					<td><a href="{{route('casefile.download', $attachment->id)}}" class="btn btn-primary btn-sm"><i class="fa fa-download"></i></a>
					<a href="{{route('casefile.edit.deleteattachment', ['id' => $casefiles->id, 'file_id' => $attachment->id] )}}" class="btn btn-danger btn-sm"  onclick="return confirm('Are you sure you want to delete this file({{$attachment->filenameNoStamp}}) ?')" ><i class="fa fa-trash"></i></a></td>
				</tr>
				</tbody>
				@endforeach
			</table>
		</div>
	</div>
</form>
<!-- Modal -->
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Add Dictionary for Tags</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

        <div class="modal-body">
	        <div class="form-group row">
			    <label for="dictionary" class="col-sm-2 col-form-label">Tag:</label>
			    <div class="col-sm-10">
			    	<input type="text" class="form-control" id="dictionaryVal" name="dictionary">
			    </div>
			</div>
        </div>
        <div class="modal-footer">
	        <button type="button" class="btn btn-secondary" data-dismiss="modal" id="closeModal">Close</button>
	        <button type="button" class="btn btn-primary" id="saveDictionary">Save changes</button>
	    </div>
    </div>
  </div>
</div>
<script type="text/javascript" src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script>
	$(document).ready(function() {
	    $('.js-example-basic-multiple').select2();

	    $('#addDictionary').click(function(){
	    	$('.modal-body').find('textarea,input').val('');
	    });

	    $('#saveDictionary').click(function() {

	    	var dictionaryVal = $('#dictionaryVal').val();

	    	if( dictionaryVal != ''){
	    		$.ajax({
				type: "POST",
				url: "../../dictionary",
				data: {dictionary:dictionaryVal},
				headers: {
			        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			    },
				success: function(data){
					console.log(data.id)
					$('#tags').append( "<option value='"+ data.id +"' selected=''>"+ data.tag +"</option>");
				}		
			})
	    	$('#dictionaryVal').val('');
	    	// $('#closeModal').click();
	    	}
	    	
	    })
	});
</script>
@endsection