@extends('layouts.app')
@section('content')
@if (session('success_alert'))
<br>
    <div class="alert alert-success">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        {{ session('success_alert') }}
    </div>
@endif
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        
        <!-- Styles -->
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css">

<br>
<div class="card">
	<div class="card-body">
		<div class="row">
			<div class="col-md-6">
				<h4>Document File</h4>
			</div>
			<div class="col-md-6">
				<a href="{{route('casefile.create')}}" class="btn btn-success float-right"><i class="fa fa-plus-circle"></i></a>
			</div>
		</div><br>
        <div class="row">
        	<div class="col">
	        	<table class="table table-striped hover table-bordered" id="table">
		            <thead>
		                <tr>
		                    <th width="15%">Document Title</th>
		                    <th>Document No.</th>
		                    <!-- <th>Regional Case No.</th> -->
		                    <th>Date</th>
		                    <!-- <th>Description</th> -->
		                    <th>Description</th>
		                    <th>Tag</th>
		                    <th>Action</th>
		                </tr>
		            </thead>
		            <tbody>
		                    @foreach($casefiles as $casefile)
		                    <tr>
		                        <td>{{$casefile->case_title}}</td>
		                        <td>{{$casefile->hlurb_case_no}}</td>
		                       <!--  <td>{{$casefile->regional_case_no}}</td> -->
		                        <td>{{$casefile->case_year}}</td>
		                        <!-- <td>{{$casefile->nature_of_case}}</td> -->
		                        <td>{{$casefile->other_description}}</td>
		                        <td>
		                        	@foreach($casefile->Tags as $tag)
										<span class="badge badge-primary">{{$tag->Dictionary->tag}}</span>
									@endforeach
		                        </td>
		                        <td>
		                        	<a href="{{ route('casefile.show', $casefile->id)}}" class="btn btn-primary btn-sm"><i class="icon-magnifier"></i></a>
		                        	<a href="{{ route('casefile.edit', $casefile->id)}}" class="btn btn-secondary btn-sm"><i class="icon-pencil"></i></a>
		                        	<!-- <div class="btn-group">
									  <button type="button" class="btn btn-sm btn-warning dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
									    More
									  </button>
									  <div class="dropdown-menu">
									    <a class="dropdown-item" href="{{ route('casefile.assign', $casefile->id)}}">Assign</a>
									  </div>
									</div> -->
		                        </td>
		                    </tr>
		                    @endforeach 
		            </tbody>
		         </table>
	        </div>	
		</div>
	</div>
</div>
<script type="text/javascript" src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript">
	 $(document).ready( function () {
	    $('#table').DataTable();
	   
	    } );

	</script>
@endsection