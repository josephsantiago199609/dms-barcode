@extends('layouts.app')
@section('content')
<br>
@if (session('success_alert'))
<br>
    <div class="alert alert-success">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        {{ session('success_alert') }}
    </div>
@endif
<div class="card">
	<div class="card-body">
		<h4>Case File Information</h4>
		<br>
		
		<table class="table table-hover" >
			<tbody>
				
				<tr>
					<th width="20%">Document Title:</th>
					<td>{{$casefiles->case_title}}</td>
				</tr>
				<tr>	
					<th>Document No:</th>
					<td>{{$casefiles->hlurb_case_no}}</td>
				</tr>
				<!-- <tr>
					<th>Regional Case No:</th>
					<td>{{$casefiles->regional_case_no}}</td>
				</tr> -->
				<tr>
					<th>Date:</th>
					<td>{{$casefiles->case_year}}</td>
				</tr>
				<!-- <tr>
					<th>Case Type:</th>
					<td>SAMPLE CASE TYPE:</td>
				</tr> -->
				<!-- <tr>
					<th>Nature of Case:</th>
					<td>{{$casefiles->nature_of_case}}</td>
				</tr> -->
				<tr>
					<th>Descriptions:</th>
					<td>{{$casefiles->other_description}}</td>
				</tr>
				<!-- <tr>
					<th>Assigned To:</th>
					<td>{{$casefiles->assigned_to}}</td>
				</tr> -->
				<tr>
					<th>Tags:</th>
					<td>
						@foreach($casefiles->Tags as $tag)
							<span class="badge badge-primary">{{$tag->Dictionary->tag}}</span>
						@endforeach
					</td>
				</tr>
				
		</table>
		<br>
		
		
		
	</div>
</div>
<div class="card">
	<div class="card-body">
		<h4>Attachments</h4>
		<br>
		<table class="table table-hover">
			<thead>
				<tr>
					<th>Filename</th>
					<th>Actions</th>
				</tr>
			</thead>
			<tbody>
				@foreach($casefilesattachments as $attachment)
				<tr>
					<td>{{$attachment->filenameNoStamp}}</td>
					<td><a href="{{route('casefile.download', $attachment->id)}}" class="btn btn-primary btn-sm"><i class="fa fa-download"></i></a></td>
				</tr>
				@endforeach
			</tbody>
		</table>
	</div>
</div>
<div class="row">
	<div class="col">
		<a href="{{ route('casefile.index')}}" class="btn btn-sm btn-danger">Back</a>
		<a href="{{ route('casefile.edit', $casefiles->id)}}" class="btn btn-sm btn-success" style="float: right;">Edit</a>
	</div>
</div>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.js"></script>

<script type="text/javascript">
	$(document).ready( function () {
    	$('#table_id').DataTable();
	} );
</script>
@endsection