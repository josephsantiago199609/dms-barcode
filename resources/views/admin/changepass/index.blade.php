@extends('layouts.app')
@section('content')
@if (session('success_alert'))
    <div class="alert alert-success">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        {{ session('success_alert') }}
    </div>
@endif
<form method="post" action="{{route('changepass.update')}}" enctype="multipart/form-data">
	{{ csrf_field() }}
	<div class="card">
		<div class="card-body">
			<div class=col-sm-12>
				<h4>Change Password</h4><br>
				<div class="form-group row">
					<label for="old_password" class="col-sm-2 col-form-label">Old Password:</label>
					<div class="col-sm-4">
						<input type="password" class="form-control" id="old_password" name="old_password" minlength="6" required>
					</div>
				</div>
				<div class="form-group row">
					<label for="new_password" class="col-sm-2 col-form-label">New Password:</label>
					<div class="col-sm-4">
						<input type="password" class="form-control" id="new_password" name="new_password" minlength="6" required>
					</div>
				</div>
				<div class="form-group row">
					<label for="confirm_password" class="col-sm-2 col-form-label">Confirm New Password:</label>
					<div class="col-sm-4">
						<input type="password" class="form-control" id="confirm_password" name="confirm_password" minlength="6" required>
					</div>
				</div>
				<div class="form-group row">
					
					<div class="col-sm-6">
						<button type="submit" class="btn-block btn btn-danger" style="float: right" id="submit">Change Password</button>
					</div>
				</div>
				<div class="row">
                <div class="col-md-5">
                    @if($errors->count())
                    <div class="alert alert-danger">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        <ul>
                            @foreach($errors->all() as $error)
                            <li>{{$error}}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                    @if (session('alert'))
                        <div class="alert alert-danger">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            {{ session('alert') }}
                        </div>
                    @endif
                </div>
            </div>
				
			</div>	
		</div>
	</div>
	
</form>
@endsection