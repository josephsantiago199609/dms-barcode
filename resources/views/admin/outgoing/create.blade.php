@extends('layouts.app')
@section('content')
@if (session('success_alert'))
    <div class="alert alert-success">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        {{ session('success_alert') }}
    </div>
@endif
<form method="post" action="{{route('outgoing.store')}}" enctype="multipart/form-data">
	{{ csrf_field() }}
	<br>
	<div class="card">
		<div class="card-body">
			<button type="submit" class="btn-sm btn btn-success" style="float: right;" id="submit">Submit</button>
			<h4>Outgoing</h4><br>
			
			 <!--  <div class="form-group row">
			    <label for="title" class="col-sm-2 col-form-label">Title:</label>
			    <div class="col-sm-10">
			      <input type="text" class="form-control" id="title" name="title">
			    </div>
			  </div> -->
			  <div class="form-group row">
			    <label for="barcode_number" class="col-sm-2 col-form-label">Barcode Number: </label>
			    <div class="col-sm-6">
			      <input type="text" class="form-control" id="barcode_number" name="barcode_number" value="{{$increment}}" readonly=""><br>
			    	 <a id="incre" class="btn btn-success" value="+"><i class="fa fa-plus-circle"></i></a>
			    	 <a id="decre" class="btn btn-danger" value="-"><i class="fa fa-minus-circle"></i></a>
			    </div>
			  </div>
			   <div class="form-group row">
			    <label for="purpose" class="col-sm-2 col-form-label">Purpose/Description:</label>
			    <div class="col-sm-10">
			      <textarea class="form-control" id="description" rows="4" name="description" required=""></textarea>
			    </div>
			  </div>
			  <div class="form-group row">
			    <label for="from" class="col-sm-2 col-form-label">To:</label>
			    <div class="col-sm-10">
			      <input type="text" class="form-control" id="from" name="from">
			    </div>
			  </div>
			  <div class="form-group row">
			    <label for="attachment" class="col-sm-2 col-form-label">Attachment:</label>
			    <div class="col-sm-10">
			      <input type="file" id="attachment" name="attachment[]" multiple=""></textarea>
			    </div>
			  </div>
			
		</div>
	</div>
	
</form>
<script type="text/javascript" src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script type="text/javascript">
	 $(document).ready( function () {
	 	var s = new Date();
	    var barnum = $('#barcode_number').val();
	    
	    var datenow = s.getFullYear().toString();
	    
	  
		
	    $('#barcode_number').val(barnum.padStart(9, datenow+'00000'));

	    $('#incre').click( function (){
	    	var a = $('#barcode_number').val();
	    	
	    	a++;
	    	$('#barcode_number').val(a.toString().padStart(9, datenow+'00000'));
	    })

	    $('#decre').click( function (){
	    	var a = $('#barcode_number').val();
	    	
	    	a--;
	    	$('#barcode_number').val(a.toString().padStart(9, datenow+'00000'));
	    })
	   }); 

	
    

</script>
@endsection