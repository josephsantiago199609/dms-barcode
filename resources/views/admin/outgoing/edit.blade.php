@extends('layouts.app')
@section('content')
@if (session('success_alert'))
    <div class="alert alert-success">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        {{ session('success_alert') }}
    </div>
@endif
<form method="post" action="{{route('outgoing.update' , $outgoingfiles->id)}}" enctype="multipart/form-data">
	{{ csrf_field() }}
	<br>
	<div class="card">	
		<div class="card-body">
			<button type="submit" class="btn-sm btn btn-success" style="float: right;" id="submit">Update</button>
			<h4>Update Information</h4><br>
			
			 	  <div class="form-group row">
			    <label for="barcode_number" class="col-sm-2 col-form-label">Barcode: </label>
			    <div class="col-sm-6">
			      {!! DNS1D::getBarcodeHTML($outgoingfiles->barcode_number, 'C128') !!}
			    </div>
			  </div>
			   <div class="form-group row">
			    <label for="purpose" class="col-sm-2 col-form-label">Purpose/Description:</label>
			    <div class="col-sm-10">
			      <textarea class="form-control" id="description" rows="4" name="description" value>{{$outgoingfiles->description}}</textarea>
			    </div>
			  </div>
			  <div class="form-group row">
			    <label for="from" class="col-sm-2 col-form-label">To:</label>
			    <div class="col-sm-10">
			      <input type="text" class="form-control" id="from" name="from" value="{{$outgoingfiles->from}}">
			    </div>
			  </div>
			  <div class="form-group row">
			    <label for="attachment" class="col-sm-2 col-form-label">Attachment:</label>
			    <div class="col-sm-10">
			      <input type="file" id="attachment" name="attachment[]" multiple></textarea>
			    </div>
			  </div>
			
		</div>
	</div>
	<div class="card">
		<div class="card-body">
			<h4>Attachments</h4><br>
			<table class="table table-hover">
				<thead>
					<tr>
						<th>Filename</th>
						<th>Actions</th>
					</tr>
				</thead>
				<tbody>
					@foreach($attachments as $attachment)
					<tr>
						<td>{{$attachment->filenameNoStamp}}</td>
						<td><a href="{{route('outgoing.download', $attachment->id)}}" class="btn btn-primary btn-sm"><i class="fa fa-download"></i></a>
						<a href="{{route('outgoing.edit.deleteattachment', ['id' => $outgoingfiles->id, 'file_id' => $attachment->id] )}}" class="btn btn-danger btn-sm"  onclick="return confirm('Are you sure you want to delete this file({{$attachment->filenameNoStamp}}) ?')" ><i class="fa fa-trash"></i></a></td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
</form>
<script type="text/javascript" src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script type="text/javascript">
	 $(document).ready( function () {
	    var test = $('#barcode_number').val();
	    console.log(test.padStart(8,'0'));
	    $('#barcode_number').val(test.padStart(8,'0'));

	  
	   }); 

	
    

</script>
@endsection