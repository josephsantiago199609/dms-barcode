@extends('layouts.app')
@section('content')
@if (session('success_alert'))

<br>
    <div class="alert alert-success">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        {{ session('success_alert') }}
    </div>
@endif
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<br>
<div class="card">
	<div class="card-body">
		<div class="row">
			<div class="col-md-6">
				<h4>Outgoing</h4>
			</div>
			<div class="col-md-6">
				<a href="{{route('outgoing.create')}}" class="btn btn-success float-right"><i class="fa fa-plus-circle"></i></a>
			</div>
		</div><br>
		<div class="row">
        	<div class="col">
	        	<table class="table table-striped hover table-bordered" id="table">
		            <thead>
		                <tr>
		                    <!-- <th>Title</th> -->
		                    <th width="30%">Barcode</th>
		               
		                    <th width="30%">Purpose/Description</th>
		                    <th>To</th>
		                    <th width="20%">Date Uploaded</th>
		                    <th>Action</th>
		                    
		                </tr>
		            </thead>
		            <tbody>
		                    @foreach($outgoingfiles as $outgoingfile)
		                    <tr>
		                        <!-- <td>{{$outgoingfile->barcode_number}}</td> -->
		                        <td><center>Office of Commissioner Fidel Exconde Jr.
		                        	{!! DNS1D::getBarcodeHTML($outgoingfile->barcode_number, 'C128',2.8,40,'black',true) !!}
		                        {{$outgoingfile->barcode_number}}</center> </td>
		                       
		                        <td>{{$outgoingfile->description}}</td>
		                        <td>{{$outgoingfile->from}}</td>
		                        <td>{{$outgoingfile->created_at}}</td>
		                        <td>
		                        	<a href="{{ route('outgoing.show', $outgoingfile->id)}}" class="btn btn-primary btn-sm"><i class="icon-magnifier"></i></a>
		                        	<a href="{{ route('outgoing.edit', $outgoingfile->id)}}" class="btn btn-secondary btn-sm"><i class="icon-pencil"></i></a>
		                        </td>
		                    </tr>
		                    @endforeach 
		            </tbody>
		         </table>
	        </div>	
		</div>
		

		
	</div>
</div>
 <script type="text/javascript" src="https://code.jquery.com/jquery-3.5.1.js"></script>
 <script type="text/javascript" src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript">
	 $(document).ready( function () {
	    $('#table').DataTable();
	   
	    } );

	</script>

@endsection