@extends('layouts.app')
@section('content')
@if (session('success_alert'))
    <div class="alert alert-success">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        {{ session('success_alert') }}
    </div>
@endif
<form method="post" action="{{route('receivingfile.store')}}" enctype="multipart/form-data">
	{{ csrf_field() }}
	<br>
	<div class="card">
		<div class="card-body">
			<button type="submit" class="btn-sm btn btn-success" style="float: right;" id="submit">Submit</button>
			<h4>Incoming</h4><br>
			
			 <!--  <div class="form-group row">
			    <label for="title" class="col-sm-2 col-form-label">Title:</label>
			    <div class="col-sm-10">
			      <input type="text" class="form-control" id="title" name="title">
			    </div>
			  </div> -->
			   <div class="form-group row">
			    <label for="purpose" class="col-sm-2 col-form-label">Purpose/Description:</label>
			    <div class="col-sm-10">
			      <textarea class="form-control" id="description" rows="4" name="description" required=""></textarea>
			    </div>
			  </div>
			  <div class="form-group row">
			    <label for="from" class="col-sm-2 col-form-label">Received From:</label>
			    <div class="col-sm-10">
			      <input type="text" class="form-control" id="from" name="from">
			    </div>
			  </div>
			  <div class="form-group row">
			    <label for="attachment" class="col-sm-2 col-form-label">Attachment:</label>
			    <div class="col-sm-10">
			      <input type="file" id="attachment" name="attachment[]" multiple=""></textarea>
			    </div>
			  </div>
			
		</div>
	</div>
	
</form>
@endsection