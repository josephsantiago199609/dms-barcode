@extends('layouts.app')
@section('content')
@if (session('success_alert'))
    <div class="alert alert-success">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        {{ session('success_alert') }}
    </div>
@endif
<form method="post" action="{{route('receivingfile.update' , $receivingfiles->id)}}"  enctype="multipart/form-data">
	{{ csrf_field() }}
	<br>
	<div class="card">	
		<div class="card-body">
			<button type="submit" class="btn-sm btn btn-success" style="float: right;" id="submit">Update</button>
			<h4>Update Information</h4><br>
			
			  <!-- <div class="form-group row">
			    <label for="title" class="col-sm-2 col-form-label">Title:</label>
			    <div class="col-sm-10">
			      <input type="text" class="form-control" id="title" name="title" value="{{$receivingfiles->title}}">
			    </div>
			  </div> -->
			   <div class="form-group row">
			    <label for="purpose" class="col-sm-2 col-form-label">Purpose/Description:</label>
			    <div class="col-sm-10">
			      <textarea class="form-control" id="description" rows="4" name="description" value>{{$receivingfiles->description}}</textarea>
			    </div>
			  </div>
			  <div class="form-group row">
			    <label for="from" class="col-sm-2 col-form-label">Received From:</label>
			    <div class="col-sm-10">
			      <input type="text" class="form-control" id="from" name="from" value="{{$receivingfiles->from}}">
			    </div>
			  </div>
			  <div class="form-group row">
			    <label for="attachment" class="col-sm-2 col-form-label">Attachment:</label>
			    <div class="col-sm-10">
			      <input type="file" id="attachment" name="attachment[]" multiple></textarea>
			    </div>
			  </div>
			
		</div>
	</div>
	<div class="card">
		<div class="card-body">
			<h4>Attachments</h4><br>
			<br>
			<table class="table table-hover">
				<thead>
					<tr>
						<th>Filename</th>
						<th>Actions</th>
					</tr>
				</thead>
				<tbody>
				@foreach($attachments as $attachment)
				<tr>
					<td>{{$attachment->filenameNoStamp}}</td>
					<td><a href="{{route('receivingfile.download', $attachment->id)}}" class="btn btn-primary btn-sm"><i class="fa fa-download"></i></a>
					<a href="{{route('receivingfile.edit.deleteattachment', ['id' => $receivingfiles->id, 'file_id' => $attachment->id] )}}" class="btn btn-danger btn-sm"  onclick="return confirm('Are you sure you want to delete this file({{$attachment->filenameNoStamp}}) ?')" ><i class="fa fa-trash"></i></a></td>
				</tr>
				</tbody>
				@endforeach
			</table>
		</div>
	</div>
</form>
@endsection