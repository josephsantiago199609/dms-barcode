@extends('layouts.app')
@section('content')
@if (session('success_alert'))
<br>
    <div class="alert alert-success">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        {{ session('success_alert') }}
    </div>
@endif
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<br>
<div class="card">
	<div class="card-body">
		<div class="row">
			<div class="col-md-6">
				<h4>Incoming</h4>
			</div>
			<div class="col-md-6">
				<a href="{{route('receivingfile.create')}}" class="btn btn-success float-right"><i class="fa fa-plus-circle"></i></a>
			</div>
		</div><br>
		<div class="row">
        	<div class="col">
	        	<table class="table table-striped hover table-bordered" id="table">
		            <thead>
		                <tr>
		                    <!-- <th>Title</th> -->
		                    <th width="50%">Purpose/Description</th>
		                    <th>From</th>
		                    <th width="10%">Date Uploaded</th>
		                    <th>Action</th>
		                    
		                </tr>
		            </thead>
		            <tbody>
		                    @foreach($receivingfiles as $receivingfile)
		                    <tr>
		                        <!-- <td>{{$receivingfile->title}}</td> -->
		                        <td>{{$receivingfile->description}}</td>
		                        <td>{{$receivingfile->from}}</td>
		                        <td>{{$receivingfile->created_at}}</td>
		                        <td>
		                        	<a href="{{ route('receivingfile.show', $receivingfile->id)}}" class="btn btn-primary btn-sm"><i class="icon-magnifier"></i></a>
		                        	<a href="{{ route('receivingfile.edit', $receivingfile->id)}}" class="btn btn-secondary btn-sm"><i class="icon-pencil"></i></a>
		                        </td>
		                    </tr>
		                    @endforeach 
		            </tbody>
		         </table>
	        </div>	
		</div>
		

		
	</div>
</div>
 <script type="text/javascript" src="https://code.jquery.com/jquery-3.5.1.js"></script>
 <script type="text/javascript" src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript">
	 $(document).ready( function () {
	    $('#table').DataTable();
	    } );

	</script>

@endsection