@extends('layouts.app')
@section('content')
<br>
@if (session('success_alert'))
    <div class="alert alert-success">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        {{ session('success_alert') }}
    </div>
@endif
<div class="card">
	<div class="card-body">
		<h4>Receiving File Information</h4>
		<br>
		
		<table class="table table-hover">
			<tbody>
				
				<!-- <tr>
					<th width="20%">Title:</th>
					<td>{{$receivingfiles->title}}</td>
				</tr> -->
				<tr>	
					<th width="20%">Purpose/Description:</th>
					<td>{{$receivingfiles->description}}</td>
				</tr>
				<tr>
					<th>From:</th>
					<td>{{$receivingfiles->from}}</td>
				</tr>
		</table>

		
		
	</div>
</div>
<div class="card">
	<div class="card-body">
		<h4>Attachments</h4>
		<br>
		<table class="table table-hover">
			<thead>
				<tr>
					<th>Filename</th>
					<th>Actions</th>
				</tr>
			</thead>
			<tbody>
				@foreach($attachments as $attachment)
				<tr>
					<td>{{$attachment->filenameNoStamp}}</td>
					<td><a href="{{route('receivingfile.download', $attachment->id)}}" class="btn btn-primary btn-sm"><i class="fa fa-download"></i></a></td>
				</tr>
				@endforeach
			</tbody>
		</table>
	</div>
</div>
<div class="row">
			<div class="col">
				<a href="{{ route('receivingfile.index')}}" class="btn btn-sm btn-danger">Back</a>
				<a href="{{ route('receivingfile.edit', $receivingfiles->id)}}" class="btn btn-sm btn-success" style="float: right;">Edit</a>
				
			</div>
		</div>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.js"></script>

<script type="text/javascript">
	$(document).ready( function () {
    	$('#table_id').DataTable();
	} );
</script>
@endsection