@extends('layouts.app')
@section('content')
@if (session('success_alert'))
<br>
    <div class="alert alert-success">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        {{ session('success_alert') }}
    </div>
@endif
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        
        <!-- Styles -->
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css">

<br>
<div class="card">
	<div class="card-body">
		<div class="row">
			<div class="col-md-6">
				<h4>Status Report</h4>
			</div>
			
		</div><br>
        <div class="row">
        	<div class="col">
	        	<table class="table table-striped hover table-bordered" id="table">
		            <thead>
		                <tr>
		                    <th width="50%">Case Title</th>
		                    <th>Current Status</th>
		                    <th>Assinged To</th>
		                    <th>Action</th>
		                    
		                </tr>
		            </thead>
		            <tbody>
		                  @foreach($casefiles as $casefile)
		                    <tr>
		                        <td>{{$casefile->case_title}}</td>
		                        <td>@if($casefile->Status)
									{{$casefile->Status->status}}
									@endif
								</td>
		                       	<td>{{$casefile->assigned_to}}</td>
		                       
		                        <td>
		                        	<a href="{{ route('reports.show', $casefile->id)}}" class="btn btn-primary btn-sm"><i class="icon-magnifier"></i></a>
		                        	
		                        </td>
		                    </tr>
		                    @endforeach 

		            </tbody>
		         </table>
	        </div>	
		</div>
	</div>
</div>
<script type="text/javascript" src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript">
	 $(document).ready( function () {
	    $('#table').DataTable();

	    } );

	</script>
@endsection