@extends('layouts.app')
@section('content')
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css">
<br>
@if (session('success_alert'))
    <div class="alert alert-success">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        {{ session('success_alert') }}
    </div>
@endif
<div class="card">
	<div class="card-body">
		<div class="row">
			<div class="col-md-6">
				<h4>Case Log</h4>
			</div>
			<div class="col-md-6">
				
				<div class="col text-right">

				   <div class="dropdown">
					  <a class="btn btn-secondary dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					    Change Status
					  </a>
					  <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
					  	@foreach($statuses as $status)
						    <a class="dropdown-item statusBtn" href="#" data-id="{{$status->id}}" data-text="{{$status->status}}" data-toggle="modal" data-target="#exampleModalCenter">{{$status->status}}</a>
						    <div class="dropdown-divider"></div>
					    @endforeach
					  </div>
					</div>
				</div><br>
		</div><br>
		
		<table class="table table-hover">
			<tbody>
				

				<tr style="border-top:solid gray">	
					<td width="20%">Case Title:</td>
					<td><b>{{$casefiles->case_title}}</b></td>
				</tr>
				<tr>
					<td>Assigned To:</td>
					<td><b>{{$casefiles->assigned_to}}</b></td>
				</tr>
				<tr style="border-bottom:solid gray">
					<td>Status:</td>
					<td>
						<b>
							@if($casefiles->Status)
								{{$casefiles->Status->status}}
							@endif
						</b>
					</td>
				</tr>
		</table>
		
	</div>
</div>

	
		<table class="table table-hover">
			<thead>
				<tr style="border-bottom:solid gray;">
					<th style="font-size: 20px">Status</th>
					<th style="font-size: 20px">Remarks</th>
					<th style="font-size: 20px">Attachments</th>
					<th style="font-size: 20px" width="200">Date</th>
					
				</tr>
			</thead>
			<tbody>
				@foreach($statusLog as $log)
					<tr>
						<td>
							@if($log->count <= 1)
								{{$log->Status->status}}
							@else
								{{$log->Status->status}} ({{$log->count}})
							@endif
						</td>
						<td>{{$log->remarks}}</td>
						<td>
							@foreach($log->Attachments as $attachment)
								<a href="{{route('reports.download', ['id'=>$casefiles->id,'attachment_id'=>$attachment->id])}}" >{{$attachment->filenameNoStamp}}</a>
							@endforeach
						</td>
						<td>{{ date('F d, Y h:i:s a', strtotime($log->created_at))}} </td>
					</tr>
				@endforeach
			</tbody>
		</table>

<div class="row">
	
	<div class="col-md-6">
		<a href="{{ route('reports.print', $casefiles->id)}}" class="btn btn-lg btn-success float-right" target="_blank">Print</a>
	</div>
</div>
	
</div>

<!-- Modal -->
<div class="modal fade exampleModalCenter" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Change Status</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form method="POST" action="{{route('reports.show.changeStatus', $casefiles->id)}}" enctype="multipart/form-data">
      	{{ csrf_field() }}
	    <div class="modal-body">
	    	<input type="hidden" name="status_id" value="" id="status_id">
	    	<div class="form-group row">
			    <label for="status" class="col-sm-2 col-form-label"><b>Status:</b></label>
			    <label for="status" class="col-sm-10 col-form-label" id="statues_text_label"></label>
			</div>
	        <div class="form-group row">
			    <label for="remarks" class="col-sm-2 col-form-label"><b>Remarks:</b></label>
			    <div class="col-sm-10">
			      <textarea class="form-control" id="remarks" name="remarks" rows="4"></textarea>
			    </div>
			</div>

			<div class="form-group row">
			    <label for="attachment" class="col-sm-2 col-form-label"><b>Attachment:</b></label>
			    <div class="col-sm-10">
			      <input type="file" id="attachment" name="attachment[]" multiple></textarea>
			    </div>
			</div>
	    </div>
      
	    <div class="modal-footer">
	        <button type="button" class="btn btn-secondary" data-dismiss="modal" id="closeModal">Close</button>
	        <button type="Submit" class="btn btn-primary">Save changes</button>
	    </div>
      </form>
    </div>
  </div>
</div>

<script type="text/javascript" src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.js"></script>
<script type="text/javascript">
	$(document).ready( function () {
    	$('#table_id').DataTable();

    	$('.statusBtn').click(function() {
    		$('.modal-body').find('textarea,input').val('');
    		var statusId = $(this).attr('data-id');
    		var statusText = $(this).attr('data-text');
    		$('#status_id').val(statusId);
    		$('#statues_text_label').html(statusText);
    		console.log(statusId);
    	});
	} );
</script>
@endsection