@extends('layouts.app')
@section('content')
@if (session('success_alert'))
    <div class="alert alert-success">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        {{ session('success_alert') }}
    </div>
@endif
<form method="post" action="{{route('status.update', $statuses->id)}}" enctype="multipart/form-data">
	{{ csrf_field() }}
	<br>
	<div class="card">
		<div class="card-body">
			<button type="submit" class="btn-sm btn btn-success" style="float: right;" id="submit">Update</button>
			<h4><b>Update</b></h4><br>

			<form>
			  <div class="form-group row">
			    <label for="status" class="col-sm-2 col-form-label"><b>Status</b></label>
			    <div class="col-sm-10">
			      <input type="text" class="form-control" id="status" name="status" value="{{$statuses->status}}">
			    </div>
			  </div>
			  

			</form>
		</div>
	</div>
	
</form>
@endsection