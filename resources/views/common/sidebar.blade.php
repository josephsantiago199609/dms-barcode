
<div class="sidebar">
  <nav class="sidebar-nav">
    <ul class="nav">
      <li class="nav-title">DMS</li>
      <li class="nav-item">
        <a class="nav-link" href="{{ route('casefile.index')}}">
          <i class="nav-icon cui-speedometer"></i>Document File
        </a>
      </li>
      <!-- <li class="nav-item">
        <a class="nav-link" href="{{ route('reports.index')}}">
          <i class="nav-icon cui-speedometer"></i>Status Report
        </a>
      </li>  --> 
      <li class="nav-title">Receiving</li>
      <li class="nav-item">
        <a class="nav-link" href="{{ route('receivingfile.index')}}">
          <i class="nav-icon cui-speedometer"></i>Incoming
        </a>
      </li> 
      <li class="nav-item">
        <a class="nav-link" href="{{ route('outgoing.index')}}">
          <i class="nav-icon cui-speedometer"></i>Outgoing
        </a>
      </li> 
      <li class="nav-title">Maintenance</li>
      <!-- <li class="nav-item">
        <a class="nav-link" href="{{ route('status.index')}}">
          <i class="nav-icon cui-speedometer"></i>Status
        </a>
      </li>  -->
      <li class="nav-item">
        <a class="nav-link" href="{{ route('audittrail.index')}}">
          <i class="nav-icon cui-speedometer"></i>User Logs
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="{{ route('backup')}}"  onclick="return confirm('Backing Up the System. Do you want to proceed?')">
          <i class="nav-icon cui-speedometer"></i>Backup Database
        </a>
      </li>
      

       
   
           
            
      
      </li>
    </ul>
  </nav>
  <button class="sidebar-minimizer brand-minimizer" type="button"></button>
</div>