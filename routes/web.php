<?php

use App\Data;
use App\Casefile;
use App\Fileattachment;
use App\Report;
use App\outgoing;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::middleware('auth')->group(function() {
	Route::get('/', 'CasefileController@index')->name('casefile.index');
	Route::get('/document', 'CasefileController@index')->name('casefile.index');
	Route::get('/document/show/{id}', 'CasefileController@show')->name('casefile.show');
	Route::get('/document/create', 'CasefileController@create')->name('casefile.create');
	Route::post('/document/store', 'CasefileController@store')->name('casefile.store');
	Route::get('/document/edit/{id}', 'CasefileController@edit')->name('casefile.edit');
	Route::get('/document/edit/{id}/deleteattachmen/{file_id}', 'CasefileController@deleteattachment')->name('casefile.edit.deleteattachment');
	Route::post('/document/update/{id}', 'CasefileController@update')->name('casefile.update');
	Route::get('/document/show/{id}/download', 'CasefileController@download')->name('casefile.download');
	Route::get('/document/assign/{id}', 'CasefileController@assign')->name('casefile.assign');
	Route::post('/document/assignupdate/{id}', 'CasefileController@assignupdate')->name('casefile.assignupdate');

	Route::get('/receiving', 'ReceivingfileController@index')->name('receivingfile.index');
	Route::get('/receiving/create', 'ReceivingfileController@create')->name('receivingfile.create');
	Route::post('/receiving/store', 'ReceivingfileController@store')->name('receivingfile.store');
	Route::get('/receiving/show/{id}', 'ReceivingfileController@show')->name('receivingfile.show');
	Route::get('/receiving/edit/{id}', 'ReceivingfileController@edit')->name('receivingfile.edit');
	Route::get('/receiving/edit/{id}/deleteattachmen/{file_id}', 'ReceivingfileController@deleteattachment')->name('receivingfile.edit.deleteattachment');
	Route::post('/receiving/update/{id}', 'ReceivingfileController@update')->name('receivingfile.update');
	Route::get('/receiving/show/{id}/download', 'ReceivingfileController@download')->name('receivingfile.download');

	Route::get('/outgoing', 'OutgoingController@index')->name('outgoing.index');
	Route::get('/outgoing/create', 'OutgoingController@create')->name('outgoing.create');
	Route::post('/outgoing/store', 'OutgoingController@store')->name('outgoing.store');	
	Route::get('/outgoing/show/{id}', 'OutgoingController@show')->name('outgoing.show');
	Route::get('/outgoing/edit/{id}', 'OutgoingController@edit')->name('outgoing.edit');
	Route::get('/outgoing/edit/{id}/deleteattachmen/{file_id}', 'OutgoingController@deleteattachment')->name('outgoing.edit.deleteattachment');
	Route::post('/outgoing/update/{id}', 'OutgoingController@update')->name('outgoing.update');
	Route::get('/outgoing/show/{id}/download', 'OutgoingController@download')->name('outgoing.download');

	Route::get('/status', 'StatusController@index')->name('status.index');
	Route::get('/status/create', 'StatusController@create')->name('status.create');
	Route::post('/status/store', 'StatusController@store')->name('status.store');
	Route::get('/status/edit/{id}', 'StatusController@edit')->name('status.edit');
	Route::post('/status/update/{id}', 'StatusController@update')->name('status.update');

	Route::get('/reports', 'ReportController@index')->name('reports.index');
	Route::get('/reports/show/{id}', 'ReportController@show')->name('reports.show');
	Route::get('/reports/print/{id}', 'ReportController@print')->name('reports.print');
	Route::post('/reports/show/{id}/changeStatus', 'ReportController@changeStatus')->name('reports.show.changeStatus');
	Route::get('/reports/show/{id}/download/{attachment_id}', 'ReportController@download')->name('reports.download');

	Route::get('/changepass', 'ChangePasswordController@index')->name('changepass.index');	
	Route::post('/changepass/update', 'ChangePasswordController@update')->name('changepass.update');

	Route::get('create', 'DisplayDataController@create');
	Route::get('index', 'DisplayDataController@index');

	Route::get('/audittrail', 'AuditTrailController@index')->name('audittrail.index');
	Route::post('/dictionary', 'DictionaryController@store')->name('dictionary.store');

	Route::get('/backup', 'AuditTrailController@backup')->name('backup');
});


// Auth::routes();
Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');

Route::get('/home', 'HomeController@index')->name('home');


